# Docker for SecurityMonkey
FROM ubuntu:16.04
MAINTAINER XXX <XXX@gmail.com>


ENV SECURITY_MONKEY_VERSION=v0.9.2 \
    SECURITY_MONKEY_SETTINGS=/usr/local/src/security_monkey/env-config/config-docker.py
#For postgres installations
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8 &&\
    echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list &&\
    apt-get update -y &&\
    apt-get -y install python-pip python-dev python-psycopg2 postgresql postgresql-contrib libpq-dev nginx supervisor git libffi-dev curl apt-transport-https gcc build-essential autoconf libtool pkg-config python-opengl python-imaging python-pyrex python-pyside.qtopengl idle-python2.7 qt4-dev-tools qt4-designer libqtgui4 libqtcore4 libqt4-xml libqt4-test libqt4-script libqt4-network libqt4-dbus python-qt4 python-qt4-gl libgle3 python-dev libssl-dev python-virtualenv

#Run as postgres user
# Run the rest of the commands as the ``postgres`` user created by the ``postgres-9.3`` package when it was ``apt-get installed``
USER postgres

# Create a PostgreSQL role named ``docker`` with ``docker`` as the password and
# then create a database `docker` owned by the ``docker`` role.
# Note: here we use ``&&\`` to run commands one after the other - the ``\``
#       allows the RUN command to span multiple lines.

RUN /etc/init.d/postgresql start &&\
    psql --command "ALTER USER postgres with PASSWORD 'securitymonkeypassword'; " &&\
    psql --command "CREATE DATABASE \"secmonkey\";" &&\
    psql --command "CREATE ROLE \"securitymonkeyuser\" LOGIN PASSWORD 'securitymonkeypassword';" &&\
    psql --command "CREATE SCHEMA secmonkey ;" &&\
    psql --command "GRANT Usage, Create ON SCHEMA \"secmonkey\" TO \"securitymonkeyuser\"; " &&\
    psql --command "set timezone TO 'GMT';" &&\
    psql --command "select now();" &&\
    echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/9.6/main/pg_hba.conf &&\
    echo "listen_addresses='*'" >> /etc/postgresql/9.6/main/postgresql.conf

# Expose the PostgreSQL port
EXPOSE 5432
EXPOSE 443

# Add VOLUMEs to allow backup of config, logs and databases
VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]

USER root
#RUN useradd -d /home/ubuntu -m -s /bin/bash ubuntu &&\
#    git clone https://github.com/Netflix/security_monkey.git /home/ubuntu/security_monkey &&\
#    cd /home/ubuntu/security_monkey && python setup.py install
RUN useradd -d /home/ubuntu -m -s /bin/bash ubuntu
RUN git clone --branch $SECURITY_MONKEY_VERSION https://github.com/Netflix/security_monkey.git /usr/local/src/security_monkey
RUN chown -R ubuntu:www-data /usr/local/src/security_monkey && ls
RUN cd /usr/local/src/security_monkey && virtualenv venv
#ENV WORKON_HOME /usr/local/src/security_monkey/venv
#RUN /bin/bash --login -c "source venv/bin/activate" && sleep 2 && python setup.py develop
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
WORKDIR /usr/local/src/security_monkey
RUN pwd && ls
RUN . venv/bin/activate; python setup.py develop

ADD securitymonkey.conf /etc/nginx/sites-available/
ADD securitymonkey.sh /usr/local/src/
ADD config-deploy.py /usr/local/src/security_monkey/env-config/
RUN curl https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN curl https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list && \
  apt-get update &&\
  apt-get install -y -q dart

RUN cd /usr/local/src/security_monkey/dart &&\
  /usr/lib/dart/bin/pub get &&\
  /usr/lib/dart/bin/pub build &&\
  /bin/mkdir -p /usr/local/src/security_monkey/security_monkey/static/ &&\
  /bin/cp -R /usr/local/src/security_monkey/dart/build/web/* /usr/local/src/security_monkey/security_monkey/static/ &&\
  /bin/cp -R /usr/local/src/security_monkey/dart/build/web/* /usr/local/src/security_monkey/security_monkey/static/ &&\
  chgrp -R www-data /usr/local/src/security_monkey

CMD /usr/local/src/securitymonkey.sh
ENTRYPOINT ["/usr/local/src/securitymonkey.sh"]

